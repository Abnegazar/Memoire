package com.memoire.wohaya.domaine;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="utilisateur", uniqueConstraints = @UniqueConstraint(columnNames = {"telephone", "password"}))
public class Utilisateur {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idUtilisateur;

    @Column(name = "nom", nullable = false)
    private String nom;

    @Column(name = "prenom", nullable = false)
    private String prenom;

    @Column(name = "telephone")
    private String telephone;

    @Column(name = "password")
    @NotNull(message = "Le mot de passe est requis.")
    private String password;

    @Column(name = "mail", unique = true)
    private String email;

    @Column(name = "sexe", nullable = false, length = 8)
    private String sexe;

    @Column(name = "photo")
    private String photos;

    @Column(name = "role", columnDefinition = "VARCHAR(50) CHECK (role in ('client', 'proprietaire', 'admin'))")
    private String role;

    @OneToMany(mappedBy = "proprietaire")
    private List<Logement> logements = new ArrayList<>();

    @OneToMany(mappedBy = "auteur")
    private List<Annonce> annonces = new ArrayList<>();

    @OneToMany(mappedBy = "destinataire")
    private List<Notification> notifications = new ArrayList<>();

    @OneToMany(mappedBy = "auteur")
    private List<Note> notes = new ArrayList<>();

    public Utilisateur() {
    }

    public Utilisateur(String nom, String prenom, String telephone, @NotNull(message = "Le mot de passe est requis.") String password, String email, String sexe, String photos, String role) {
        this.nom = nom;
        this.prenom = prenom;
        this.telephone = telephone;
        this.password = password;
        this.email = email;
        this.sexe = sexe;
        this.photos = photos;
        this.role = role;
    }

    public Long getIdUtilisateur() {
        return idUtilisateur;
    }

    public void setIdUtilisateur(Long idUtilisateur) {
        this.idUtilisateur = idUtilisateur;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSexe() {
        return sexe;
    }

    public void setSexe(String sexe) {
        this.sexe = sexe;
    }

    public String getPhotos() {
        return photos;
    }

    public void setPhotos(String photos) {
        this.photos = photos;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public List<Logement> getLogements() {
        return logements;
    }

    public void setLogements(List<Logement> logements) {
        this.logements = logements;
    }

    public List<Annonce> getAnnonces() {
        return annonces;
    }

    public void setAnnonces(List<Annonce> annonces) {
        this.annonces = annonces;
    }

    public List<Notification> getNotifications() {
        return notifications;
    }

    public void setNotifications(List<Notification> notifications) {
        this.notifications = notifications;
    }

    public List<Note> getNotes() {
        return notes;
    }

    public void setNotes(List<Note> notes) {
        this.notes = notes;
    }
}
