package com.memoire.wohaya.domaine;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "zone")
public class Zone {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idZone;

    @Column(name = "nom_zone")
    @NotNull(message = "Le nom de la zone est requis.")
    private String nomZone;

    @ManyToOne
    @JoinColumn(name = "quartier", nullable = false)
    private Quartier quartier;

    @Column(name = "type_zone", nullable = false)
    @NotNull(message = "Le type de la zone est requis.")
    private String typeZone;

    @OneToMany(mappedBy = "zone")
    private List<Localisation> localisations = new ArrayList<>();

    public Zone() {
    }

    public Zone(@NotNull(message = "Le nom de la zone est requis.") String nomZone, Quartier quartier, @NotNull(message = "Le type de la zone est requis.") String typeZone) {
        this.nomZone = nomZone;
        this.quartier = quartier;
        this.typeZone = typeZone;
    }

    public Long getIdZone() {
        return idZone;
    }

    public void setIdZone(Long idZone) {
        this.idZone = idZone;
    }

    public String getNomZone() {
        return nomZone;
    }

    public void setNomZone(String nomZone) {
        this.nomZone = nomZone;
    }

    public Quartier getQuartier() {
        return quartier;
    }

    public void setQuartier(Quartier quartier) {
        this.quartier = quartier;
    }

    public String getTypeZone() {
        return typeZone;
    }

    public void setTypeZone(String typeZone) {
        this.typeZone = typeZone;
    }

    public List<Localisation> getLocalisations() {
        return localisations;
    }

    public void setLocalisations(List<Localisation> localisations) {
        this.localisations = localisations;
    }
}
