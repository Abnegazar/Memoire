package com.memoire.wohaya.domaine;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Entity
@Table(name = "notification")
public class Notification {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idNotifications;

    @Column(name = "objet", nullable = false)
    private String objet;

    @Column(name = "contenu")
    @NotNull(message = "Cette notification semble être vide.")
    private String contenu;

    @Column(name = "date_notification", nullable = false)
    @JsonFormat(pattern = "dd/MM/yyyy")
    private LocalDate dateNotification;

    @OneToOne
    @JoinColumn(name = "destinataire")
    private Utilisateur destinataire;

    public Notification() {
    }

    public Notification(String objet, @NotNull(message = "Cette notification semble être vide.") String contenu, LocalDate dateNotification, Utilisateur destinataire) {
        this.objet = objet;
        this.contenu = contenu;
        this.dateNotification = dateNotification;
        this.destinataire = destinataire;
    }

    public Long getIdNotifications() {
        return idNotifications;
    }

    public void setIdNotifications(Long idNotifications) {
        this.idNotifications = idNotifications;
    }

    public String getObjet() {
        return objet;
    }

    public void setObjet(String objet) {
        this.objet = objet;
    }

    public String getContenu() {
        return contenu;
    }

    public void setContenu(String contenu) {
        this.contenu = contenu;
    }

    public LocalDate getDateNotification() {
        return dateNotification;
    }

    public void setDateNotification(LocalDate dateNotification) {
        this.dateNotification = dateNotification;
    }

    public Utilisateur getDestinataire() {
        return destinataire;
    }

    public void setDestinataire(Utilisateur destinataire) {
        this.destinataire = destinataire;
    }
}
