package com.memoire.wohaya.domaine;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "ville")
public class Ville {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idVille;

    @Column(name = "nom_ville", unique = true)
    @NotNull(message = "Le nom de la ville est requis")
    private String nomVille;

    @OneToMany(mappedBy = "ville")
    private List<Quartier> quartiers = new ArrayList<>();

    @OneToMany(mappedBy = "ville")
    private List<Localisation> localisations = new ArrayList<>();

    public Ville() {
    }

    public Ville(@NotNull(message = "Le nom de la ville est requis") String nomVille) {
        this.nomVille = nomVille;
    }

    public Long getIdVille() {
        return idVille;
    }

    public void setIdVille(Long idVille) {
        this.idVille = idVille;
    }

    public String getNomVille() {
        return nomVille;
    }

    public void setNomVille(String nomVille) {
        this.nomVille = nomVille;
    }
}
