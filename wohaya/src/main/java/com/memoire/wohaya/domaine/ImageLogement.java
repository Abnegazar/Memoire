package com.memoire.wohaya.domaine;

import javax.persistence.*;

@Entity
@Table(name = "image_logement")
public class ImageLogement {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idImage;

    @ManyToOne
    @JoinColumn(name = "logement")
    private Logement logement;

    @Column(name = "chemin_image", nullable = false, unique = true)
    private String cheminImage;

    public ImageLogement() {
    }

    public ImageLogement(Logement logement, String cheminImage) {
        this.logement = logement;
        this.cheminImage = cheminImage;
    }

    public Logement getLogement() {
        return logement;
    }

    public void setLogement(Logement logement) {
        this.logement = logement;
    }

    public String getCheminImage() {
        return cheminImage;
    }

    public void setCheminImage(String cheminImage) {
        this.cheminImage = cheminImage;
    }
}
