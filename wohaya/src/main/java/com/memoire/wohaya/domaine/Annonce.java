package com.memoire.wohaya.domaine;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "annonce")
public class Annonce {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idAnnonce;

    @Column(name = "objet", nullable = false)
    private String objet;

    @Column(name = "date_annonce", nullable = false)
    @JsonFormat(pattern = "jj/MM/yyyy")
    private LocalDate dateAnnonce;

    @Column(name = "prix_min")
    private float prixMin;

    @Column(name = "prix_max")
    private float prixMax;

    @Column(name = "nbr_chambre", nullable = false)
    private int nbrChambre;

    @Column(name = "type_location", nullable = false)
    private String typeLocation;

    @OneToOne
    @JoinColumn(name = "localisation")
    private Localisation localisation;

    @OneToOne
    @JoinColumn(name = "auteur")
    private Utilisateur auteur;

    public Annonce() {
    }

    public Annonce(String objet, LocalDate dateAnnonce, float prixMin, float prixMax, int nbrChambre, String typeLocation, Localisation localisation, Utilisateur auteur) {
        this.objet = objet;
        this.dateAnnonce = dateAnnonce;
        this.prixMin = prixMin;
        this.prixMax = prixMax;
        this.nbrChambre = nbrChambre;
        this.typeLocation = typeLocation;
        this.localisation = localisation;
        this.auteur = auteur;
    }

    public Long getIdAnnonce() {
        return idAnnonce;
    }

    public void setIdAnnonce(Long idAnnonce) {
        this.idAnnonce = idAnnonce;
    }

    public String getObjet() {
        return objet;
    }

    public void setObjet(String objet) {
        this.objet = objet;
    }

    public LocalDate getDateAnnonce() {
        return dateAnnonce;
    }

    public void setDateAnnonce(LocalDate dateAnnonce) {
        this.dateAnnonce = dateAnnonce;
    }

    public float getPrixMin() {
        return prixMin;
    }

    public void setPrixMin(float prixMin) {
        this.prixMin = prixMin;
    }

    public float getPrixMax() {
        return prixMax;
    }

    public void setPrixMax(float prixMax) {
        this.prixMax = prixMax;
    }

    public int getNbrChambre() {
        return nbrChambre;
    }

    public void setNbrChambre(int nbrChambre) {
        this.nbrChambre = nbrChambre;
    }

    public String getTypeLocation() {
        return typeLocation;
    }

    public void setTypeLocation(String typeLocation) {
        this.typeLocation = typeLocation;
    }

    public Localisation getLocalisation() {
        return localisation;
    }

    public void setLocalisation(Localisation localisation) {
        this.localisation = localisation;
    }

    public Utilisateur getAuteur() {
        return auteur;
    }

    public void setAuteur(Utilisateur auteur) {
        this.auteur = auteur;
    }
}
