package com.memoire.wohaya.domaine;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.io.Serializable;

@Entity
@DiscriminatorValue("chambre_classique")
public class ChambreClassique extends Logement implements Serializable {

    @Column(nullable = false)
    private int nbrPieces;

    @Column(nullable = false)
    private boolean cuisineInterne;

    @Column(nullable = false)
    private boolean douche_interne;

    public ChambreClassique() {
    }

    public ChambreClassique(int nbrPieces, boolean cuisineInterne, boolean douche_interne) {
        this.nbrPieces = nbrPieces;
        this.cuisineInterne = cuisineInterne;
        this.douche_interne = douche_interne;
    }

    public ChambreClassique(String categorie, String typeBien, Utilisateur proprietaire, Localisation adresse, int nbrPieces, boolean cuisineInterne, boolean douche_interne) {
        super(categorie, typeBien, proprietaire, adresse);
        this.nbrPieces = nbrPieces;
        this.cuisineInterne = cuisineInterne;
        this.douche_interne = douche_interne;
    }

    public int getNbrPieces() {
        return nbrPieces;
    }

    public void setNbrPieces(int nbrPieces) {
        this.nbrPieces = nbrPieces;
    }

    public boolean isCuisineInterne() {
        return cuisineInterne;
    }

    public void setCuisineInterne(boolean cuisineInterne) {
        this.cuisineInterne = cuisineInterne;
    }

    public boolean isDouche_interne() {
        return douche_interne;
    }

    public void setDouche_interne(boolean douche_interne) {
        this.douche_interne = douche_interne;
    }
}
