package com.memoire.wohaya.domaine;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "reservation")
public class Reservation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idReservation;

    @Column(name = "date_debut", nullable = false)
    @JsonFormat(pattern = "dd/MM/yyyy")
    private LocalDate dateDebut;

    @Column(name = "date_fin", nullable = false)
    @JsonFormat(pattern = "dd/MM/yyyy")
    private LocalDate dateFin;

    @Column(name = "date_reservation", nullable = false)
    @JsonFormat(pattern = "dd/MM/yyyy")
    private LocalDate dateReservation;

    @Column(name = "etat", nullable = false)
    private String etat;

    @OneToOne
    @JoinColumn(name = "logement")
    private Logement logement;

    @OneToOne
    @JoinColumn(name = "client")
    private Utilisateur client;

    public Reservation() {
    }

    public Reservation(LocalDate dateDebut, LocalDate dateFin, LocalDate dateReservation, String etat, Logement logement, Utilisateur client) {
        this.dateDebut = dateDebut;
        this.dateFin = dateFin;
        this.dateReservation = dateReservation;
        this.etat = etat;
        this.logement = logement;
        this.client = client;
    }

    public Long getIdReservation() {
        return idReservation;
    }

    public void setIdReservation(Long idReservation) {
        this.idReservation = idReservation;
    }

    public LocalDate getDateDebut() {
        return dateDebut;
    }

    public void setDateDebut(LocalDate dateDebut) {
        this.dateDebut = dateDebut;
    }

    public LocalDate getDateFin() {
        return dateFin;
    }

    public void setDateFin(LocalDate dateFin) {
        this.dateFin = dateFin;
    }

    public LocalDate getDateReservation() {
        return dateReservation;
    }

    public void setDateReservation(LocalDate dateReservation) {
        this.dateReservation = dateReservation;
    }

    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }

    public Logement getLogement() {
        return logement;
    }

    public void setLogement(Logement logement) {
        this.logement = logement;
    }

    public Utilisateur getClient() {
        return client;
    }

    public void setClient(Utilisateur client) {
        this.client = client;
    }
}
