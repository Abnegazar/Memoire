package com.memoire.wohaya.domaine;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "quartier")
public class Quartier {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idQuartier;

    @Column(name = "nom_quartier")
    @NotNull(message = "Le nom du quartier est requis")
    private String nomQuartier;

    @ManyToOne
    @JoinColumn(name = "ville", nullable = false)
    private Ville ville;

    @OneToMany(mappedBy = "quartier")
    private List<Zone> zones = new ArrayList<>();

    @OneToMany(mappedBy = "quartier")
    private List<Localisation> localisations = new ArrayList<>();

    public Quartier() {
    }

    public Quartier(@NotNull(message = "Le nom du quartier est requis") String nomQuartier, Ville ville) {
        this.nomQuartier = nomQuartier;
        this.ville = ville;
    }

    public Long getIdQuartier() {
        return idQuartier;
    }

    public void setIdQuartier(Long idQuartier) {
        this.idQuartier = idQuartier;
    }

    public String getNomQuartier() {
        return nomQuartier;
    }

    public void setNomQuartier(String nomQuartier) {
        this.nomQuartier = nomQuartier;
    }

    public Ville getVille() {
        return ville;
    }

    public void setVille(Ville ville) {
        this.ville = ville;
    }

    public List<Zone> getZones() {
        return zones;
    }

    public void setZones(List<Zone> zones) {
        this.zones = zones;
    }

    public List<Localisation> getLocalisations() {
        return localisations;
    }

    public void setLocalisations(List<Localisation> localisations) {
        this.localisations = localisations;
    }
}
