package com.memoire.wohaya.domaine;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.io.Serializable;

@Entity
@DiscriminatorValue("appartement")
public class Appartement extends Logement implements Serializable {

    @Column(nullable = false)
    private boolean meubler;

    public Appartement() {
    }

    public Appartement(boolean meubler) {
        this.meubler = meubler;
    }

    public Appartement(String categorie, String typeBien, Utilisateur proprietaire, Localisation adresse, boolean meubler) {
        super(categorie, typeBien, proprietaire, adresse);
        this.meubler = meubler;
    }

    public boolean isMeubler() {
        return meubler;
    }

    public void setMeubler(boolean meubler) {
        this.meubler = meubler;
    }
}
