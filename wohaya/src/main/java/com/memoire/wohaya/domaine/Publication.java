package com.memoire.wohaya.domaine;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "publication")
public class Publication {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idPublication;

    @Column(name = "status", nullable = false)
    private String status;

    @Column(name = "date_publication", nullable = false)
    @JsonFormat(pattern = "dd/MM/yyyy")
    private LocalDate datePublication;

    @OneToOne
    @JoinColumn(name = "logement", nullable = false)
    private Logement logement;

    public Publication() {
    }

    public Publication(String status, LocalDate datePublication, Logement logement) {
        this.status = status;
        this.datePublication = datePublication;
        this.logement = logement;
    }

    public Long getIdPublication() {
        return idPublication;
    }

    public void setIdPublication(Long idPublication) {
        this.idPublication = idPublication;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public LocalDate getDatePublication() {
        return datePublication;
    }

    public void setDatePublication(LocalDate datePublication) {
        this.datePublication = datePublication;
    }

    public Logement getLogement() {
        return logement;
    }

    public void setLogement(Logement logement) {
        this.logement = logement;
    }
}
