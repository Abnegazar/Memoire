package com.memoire.wohaya.domaine;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Inheritance(strategy=InheritanceType.JOINED)
@DiscriminatorColumn(name="type_logement")
@DiscriminatorValue("logement")
public class Logement implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idLogement;

    @Column(name = "categorie", nullable = false)
    private String categorie;

    @Column(name = "type_logement", nullable = false)
    private String typeLogement;

    @Column(name = "nbr_chambre", nullable = false)
    private int nbrChambre;

    @Column(name = "nbr_cuisine", nullable = false)
    private int nbrCuisine;

    @Column(name = "nbr_salle_bain", nullable = false)
    private int nbrSalleBain;

    @OneToOne(optional = false)
    @JoinColumn(name = "proprietaire")
    private Utilisateur proprietaire;

    @OneToOne
    @JoinColumn(name = "adresse")
    private Localisation adresse;

    @OneToMany(mappedBy = "logement", orphanRemoval = true)
    private List<ImageLogement> imageLogements = new ArrayList<>();

    @OneToMany(mappedBy = "logement", orphanRemoval = true)
    private List<Reservation> reservations;

    @OneToMany(mappedBy = "logement", orphanRemoval = true)
    private List<Publication> publications = new ArrayList<>();

    public Logement() {
    }

    public Logement(String categorie, String typeLogement, Utilisateur proprietaire, Localisation adresse) {
        this.categorie = categorie;
        this.typeLogement = typeLogement;
        this.proprietaire = proprietaire;
        this.adresse = adresse;
    }

    public Long getIdLogement() {
        return idLogement;
    }

    public void setIdLogement(Long idLogement) {
        this.idLogement = idLogement;
    }

    public String getCategorie() {
        return categorie;
    }

    public void setCategorie(String categorie) {
        this.categorie = categorie;
    }

    public String getTypeLogement() {
        return typeLogement;
    }

    public void setTypeLogement(String typeLogement) {
        this.typeLogement = typeLogement;
    }

    public Utilisateur getProprietaire() {
        return proprietaire;
    }

    public void setProprietaire(Utilisateur proprietaire) {
        this.proprietaire = proprietaire;
    }

    public Localisation getLocalisation() {
        return adresse;
    }

    public void setLocalisation(Localisation adresse) {
        this.adresse = adresse;
    }

    public List<ImageLogement> getImageLogements() {
        return imageLogements;
    }

    public void setImageLogements(List<ImageLogement> imageLogements) {
        this.imageLogements = imageLogements;
    }

    public List<Reservation> getReservations() {
        return reservations;
    }

    public void setReservations(List<Reservation> reservations) {
        this.reservations = reservations;
    }

    public List<Publication> getPublications() {
        return publications;
    }

    public void setPublications(List<Publication> publications) {
        this.publications = publications;
    }
}
