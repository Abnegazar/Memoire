package com.memoire.wohaya.domaine;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "localisation")
public class Localisation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idLocalisation;

    @OneToOne(optional = false)
    @JoinColumn(name = "ville")
    private Ville ville;

    @OneToOne(optional = false)
    @JoinColumn(name = "quartier")
    private Quartier quartier;

    @OneToOne
    @JoinColumn(name = "zone")
    private Zone zone;

    @OneToMany(mappedBy = "adresse")
    private List<Logement> logements = new ArrayList<>();

    @OneToMany(mappedBy = "localisation")
    private List<Annonce> annonces = new ArrayList<>();

    public Localisation() {
    }

    public Localisation(Ville ville, Quartier quartier, Zone zone) {
        this.ville = ville;
        this.quartier = quartier;
        this.zone = zone;
    }

    public Long getIdLocalisation() {
        return idLocalisation;
    }

    public void setIdLocalisation(Long idLocalisation) {
        this.idLocalisation = idLocalisation;
    }

    public Ville getVille() {
        return ville;
    }

    public void setVille(Ville ville) {
        this.ville = ville;
    }

    public Quartier getQuartier() {
        return quartier;
    }

    public void setQuartier(Quartier quartier) {
        this.quartier = quartier;
    }

    public Zone getZone() {
        return zone;
    }

    public void setZone(Zone zone) {
        this.zone = zone;
    }

    public List<Logement> getLogements() {
        return logements;
    }

    public void setLogements(List<Logement> logements) {
        this.logements = logements;
    }

    public List<Annonce> getAnnonces() {
        return annonces;
    }

    public void setAnnonces(List<Annonce> annonces) {
        this.annonces = annonces;
    }
}
