package com.memoire.wohaya.domaine;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.io.Serializable;

@Entity
@DiscriminatorValue("residence")
public class Residence extends Logement implements Serializable {

    @Column(name = "piscine", nullable = false)
    private boolean piscine;

    @Column(name = "jardin", nullable = false)
    private boolean jardin;

    public Residence() {
    }

    public Residence(boolean piscine, boolean jardin) {
        this.piscine = piscine;
        this.jardin = jardin;
    }

    public Residence(String categorie, String typeBien, Utilisateur proprietaire, Localisation adresse, boolean piscine, boolean jardin) {
        super(categorie, typeBien, proprietaire, adresse);
        this.piscine = piscine;
        this.jardin = jardin;
    }

    public boolean isPiscine() {
        return piscine;
    }

    public void setPiscine(boolean piscine) {
        this.piscine = piscine;
    }

    public boolean isJardin() {
        return jardin;
    }

    public void setJardin(boolean jardin) {
        this.jardin = jardin;
    }
}
