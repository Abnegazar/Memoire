package com.memoire.wohaya.domaine;

import javax.persistence.*;

@Entity
@Table(name = "note")
public class Note {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idNote;

    @Column(name = "valeur", nullable = false)
    private int valeur;

    @OneToOne
    @JoinColumn(name = "auteur")
    private Utilisateur auteur;

    @OneToOne
    @JoinColumn(name = "publication")
    private Publication publication;

    public Note() {
    }

    public Note(int valeur, Utilisateur auteur, Publication publication) {
        this.valeur = valeur;
        this.auteur = auteur;
        this.publication = publication;
    }

    public Long getIdNote() {
        return idNote;
    }

    public void setIdNote(Long idNote) {
        this.idNote = idNote;
    }

    public int getValeur() {
        return valeur;
    }

    public void setValeur(int valeur) {
        this.valeur = valeur;
    }

    public Utilisateur getAuteur() {
        return auteur;
    }

    public void setAuteur(Utilisateur auteur) {
        this.auteur = auteur;
    }

    public Publication getPublication() {
        return publication;
    }

    public void setPublication(Publication publication) {
        this.publication = publication;
    }
}
